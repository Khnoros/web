<!DOCTYPE html> 
<html lang="pt-BR"> 
<head> 
  <meta charset="utf-8"> 
  <title> </title> 
	<link rel="stylesheet" href="quizz.css">
</head> 

<!-- - Criar um fieldset para cada pergunta 
	 - input type required para preencher o campo
	 - Para escolher apenas UMA opção usamos o TYPE="RADIO" 
	 Se for mais de uma opção usamos o TYPE="CHECKBOX"
-->

<body> 
	
	<h1> Programação Orientada - Quizz - Testando </h1>
<form action="index.php" method="post">	
<fieldset>
	<legend>  Pergunta 1 </legend>
	
	<label class="alinha"> Quais os elementos da Programação Orientada ? </label><br>	<br>
	<input type="radio" name="primeira" value="1" required /> Classes, Objetos, Atributos, Métodos e Construtores <br>
	<input type="radio" name="primeira" value="2"> Classes, Objetos, Atributos, Métodos e Tags.<br>
	<input type="radio" name="primeira" value="3"> Classes, Atributos, Tags e subprogramação.<br>
	<input type="radio" name="primeira" value="4"> Classes, Atributos, Tags , subprogramação e Métodos.<br>
	<input type="radio" name="primeira" value="5"> Classes, Tags, Métodos e subprogramação.
	
</fieldset>

<fieldset>
	<legend>  Pergunta 2 </legend>
	
	<label class = "alinha" > O que é <u>classe</u> em Programação Orientada ? </label><br>	<br>
	<input type="radio" name="segunda" value="6"  required /> As classes possuem só comportamentos.<br>
	<input type="radio" name="segunda" value="7"> As classes têm características e comportamentos.<br>
	<input type="radio" name="segunda" value="8"> As classes possuem só características.<br>
	<input type="radio" name="segunda" value="9"> As classes são propriedades de um objeto.<br>
	<input type="radio" name="segunda" value="10"> As classes inicializa seus atributos toda vez que é instanciado.
	
</fieldset>

<fieldset>
	<legend>  Pergunta 3 </legend>
	
	<label class ="alinha"> Quais são as caracteristicas da <u>classe</u> ? </label><br>	<br>
	<input type="radio" name="terceira" value="11"  required /> Possuem somente visibilidade. <br>
	<input type="radio" name="terceira" value="12"> Possuem um nome , visibilidade , características e ações.<br>
	<input type="radio" name="terceira" value="13"> Possuem visibilidade, características e atributos.<br>
	<input type="radio" name="terceira" value="14"> Possuem características , atributos e ações.<br>
	<input type="radio" name="terceira" value="15"> Possuem atributos e ações.
	
</fieldset>

<fieldset>
	<legend>  Pergunta 4 </legend>
	
	<label class ="alinha"> O que são <u>atributos</u> em Programação Orientada  ? </label><br>	<br>
	<input type="radio" name="quarto" value="16"  required /> São comportamentos de um objeto. <br>
	<input type="radio" name="quarto" value="17"> São ações de um objeto.<br>
	<input type="radio" name="quarto" value="18"> São caracteristicas de um objetos.<br>
	<input type="radio" name="quarto" value="19"> São métodos de um objeto.<br>
	<input type="radio" name="quarto" value="20"> São as propriedades de um objeto .
	
</fieldset>

<fieldset>
	<legend>  Pergunta 5 </legend>
	
	<label class ="alinha"> O que são <u>construtores</u> em Programação Orientada  ? </label><br>	<br>
	<input type="radio" name="quinto" value="21"  required /> Inicializam os métodos. <br>
	<input type="radio" name="quinto" value="22"> Construtores inicializam os comportamentos do objeto.<br>
	<input type="radio" name="quinto" value="23"> Métodos que se inicializam com os construtores.<br>
	<input type="radio" name="quinto" value="24"> Inicializam seus atributos toda vez que é instanciado.<br>
	<input type="radio" name="quinto" value="25"> Construtores executam seus atributos com o objeto.
	
</fieldset>




			<button type = "submit" name = "enviar" > Mostrar  </button>

<?php 
if(isset($_POST["enviar"])){
	
	require "quizz.inc.php";
	
	// Recebendo os dados do HTML
	$primeira = $_POST["primeira"];
	$segunda = $_POST["segunda"];
	$terceira = $_POST["terceira"];
	$quarto = $_POST["quarto"];
	$quinto = $_POST["quinto"];

	$pontos = perguntas($primeira,$segunda,$terceira,$quarto,$quinto);
	mostrar($pontos);
	


}
?>
	
</body> 
</html> 